The callback redirect URI used by SQLPad is <baseurl>/auth/oidc/callback.

For the above configuration, assuming SQLPAD_BASE_URL = "/sqlpad", the callback URI configured with the provider should be http://localhost:3010/sqlpad/auth/oidc/callback.

The contents of the OpenID sign in button can be customized with the following

#snippet
SQLPAD_OIDC_LINK_HTML = "text or inner html here"

Prior to authenticating via OpenID, users must still be added to SQLPad with their email address used to log in.

This can be bypassed by using allowed domains to auto-add users for emails belonging to certain domains.

# space delimited list of domains to allow
SQLPAD_ALLOWED_DOMAINS = "mycompany.com"




---------------




# localhost used in dev
PUBLIC_URL = "http://localhost:3010"

# HTML code for the sign-in link used for starting Open ID authentication.
SQLPAD_OIDC_LINK_HTML = "Sign in with OpenID"

SQLPAD_OIDC_CLIENT_ID = "actual-client-id"
SQLPAD_OIDC_CLIENT_SECRET = "actual-client-secret"

# Authentication scope allows to customize the scope depend on the supported provider.
# Default value is "openid profile email roles"
SQLPAD_OIDC_SCOPE = "openid profile email roles"

# Issuer endpoint (will vary by provider)
# As of version 6.4.0 the issuer endpoint is the only URL needed
# as long as the OIDC provider supplies .well-known endpoints
SQLPAD_OIDC_ISSUER = "https://some.openidprovider.com/oauth2/default"

# If .well-known is not supported, the additional 3 endpoints must be supplied
# Supplying these endpoints is not recommended,
# as it uses an older openid implementation which may not be compatible with your auth provider.
SQLPAD_OIDC_AUTHORIZATION_URL = "https://some.openidprovider.com/oauth2/default/v1/authorize"
SQLPAD_OIDC_TOKEN_URL = "https://some.openidprovider.com/oauth2/default/v1/token"
SQLPAD_OIDC_USER_INFO_URL = "https://some.openidprovider.okta.com/oauth2/default/v1/userinfo"

# To enable spec compliant browsers to work.
# Default is 'strict' and that removes the session cookie on redirects.
SQLPAD_SESSION_COOKIE_SAME_SITE = 'Lax'
